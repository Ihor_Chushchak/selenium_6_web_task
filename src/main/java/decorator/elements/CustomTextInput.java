package decorator.elements;

import decorator.BasicElement;
import org.openqa.selenium.WebElement;

public class CustomTextInput extends BasicElement {

    public CustomTextInput(WebElement webElement) {
        super(webElement);
    }
    public void sendKeys(String text) {
        super.sendKeys(text);
    }
}