package decorator.elements;

import decorator.BasicElement;
import org.openqa.selenium.WebElement;

public class CustomLabel extends BasicElement {

    public CustomLabel(WebElement webElement) {
        super(webElement);
    }

    public String getText() {
       return super.getText();
    }
}