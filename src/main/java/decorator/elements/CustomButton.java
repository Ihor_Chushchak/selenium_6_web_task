package decorator.elements;

import decorator.BasicElement;
import org.openqa.selenium.WebElement;

public class CustomButton extends BasicElement {

    public CustomButton(WebElement webElement) {
        super(webElement);
    }

    public void click() {
        super.click();
    }

}