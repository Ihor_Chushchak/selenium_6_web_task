package bo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import po.AuthorizationPage;

public class AuthorizationPageBO {
    private static final Logger LOG = LogManager.getLogger(AuthorizationPage.class);
    private AuthorizationPage authorizationPage;

    public AuthorizationPageBO(){
        authorizationPage = new AuthorizationPage();
    }

    public void enterLoginToGmailPage(String login){
        authorizationPage.fillLogin(login);
        authorizationPage.clickLoginNextButton();
    }

    public void enterPasswordToGmailPage(String password){
        authorizationPage.fillPassword(password);
        authorizationPage.clickPasswordNextButton();
    }
}
