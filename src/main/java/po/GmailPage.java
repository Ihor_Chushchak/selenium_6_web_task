package po;

import decorator.elements.CustomButton;
import decorator.elements.CustomTextInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class GmailPage extends BasicPage {

    private static final Logger LOG = LogManager.getLogger(GmailPage.class);

    @FindBy(css = "div.aic div[role='button']")
    private CustomButton composeEmailButton;

    @FindBy(css = "textarea[name='to']")
    private CustomTextInput toField;

    @FindBy(css = "input[name='subjectbox']")
    private CustomTextInput subjectField;

    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private CustomTextInput messageField;

    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private CustomButton sendEmailButton;

    @FindBy(xpath = "//*[@id='link_vsm']")
    private CustomButton openMessageButton;

    @FindBy(xpath = "//a[@href=\"https://mail.google.com/mail/u/0/#sent\"]")
    private CustomButton chooseSendButton;

    @FindBy(xpath = "//div[@class='iH bzn']//div[@class='T-I J-J5-Ji nX T-I-ax7 T-I-Js-Gs mA']")
    private CustomButton deleteEmailButton;

    public void clickComposeEmailButton() {
        LOG.info("Clicking compose_email CustomTextInput");
        composeEmailButton.click();
    }

    public void fillToField(String to) {
        LOG.info("Filling \"To\" filed");
        toField.sendKeys(to);
    }

    public void fillSubjectField(String subject) {
        LOG.info("Filling \"Subject\" filed");
        subjectField.sendKeys(subject);
    }

    public void fillMessageField(String message) {
        LOG.info("Filling \"Message\" filed");
        messageField.sendKeys(message);
    }

    public void clickSendEmailButton() {
        LOG.info("Clicking send_email button");
        sendEmailButton.click();
    }

    public void clickOpenMessageButton() {
        LOG.info("Clicking open_sended_email button");
        openMessageButton.click();
    }

    public String getSendButtonAttribute(){
     return chooseSendButton.getAttribute("tabindex");
    }

    public void clickDeleteEmailButton(){
        LOG.info("Clicking delete_sended_email button");
        deleteEmailButton.click();
    }

    public boolean checkEmailTimeSending() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime localDateTime = LocalDateTime.now();
        String actualTime = dateTimeFormatter.format(localDateTime) + " (0 хвилин тому)";
        return Objects.equals(actualTime,driver.findElement(By.xpath("//span[@class='g3'][@role='gridcell']")).getText());

    }

    public boolean checkEmailButton(){
        return Objects.equals(getSendButtonAttribute(),"0");
    }
}
